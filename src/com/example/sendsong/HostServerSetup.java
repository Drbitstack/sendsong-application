package com.example.sendsong;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import android.net.Uri;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Looper;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class HostServerSetup extends ActionBarActivity {

	protected static final int CHOOSE_FILE_RESULT_CODE = 20;

	private static final int SERVER_PORT = 6801;

	private static final String TAG = "HostServerSetup";
	private Looper mWFDLooper;
	
	//FOR WIFI P2P
	private WifiP2pManager manager;
    private Channel channel;
    private Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//HandlerThread handlerThread = new HandlerThread(TAG);
		//handlerThread.start();
		//mWFDLooper = handlerThread.getLooper();
		setContentView(R.layout.activity_host_server_setup);
		
		initialize_wifiP2p();
		
		
		context = this;
	    manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
	    channel = manager.initialize(this, getMainLooper(),//mWFDLooper, 
	    		new WifiP2pManager.ChannelListener() {
					
					@Override
					public void onChannelDisconnected() {
						// TODO Auto-generated method stub
						Log.d(TAG, "Host: OnChannelDisconnected!");
						mWFDLooper.quit();						
					}
				});
		
        Button brose_for_playlist = (Button) findViewById(R.id.host_button_browse_for_playlist);
        Button server_toggle = (Button) findViewById(R.id.host_button_server_toggle);
        brose_for_playlist.setOnClickListener(
                new View.OnClickListener() {

                    public void onClick(View v) {
                        // Allow user to pick an image from Gallery or other
                        // registered apps
                    	playListBrowseAction();
                    }

                });
        server_toggle.setOnClickListener( 
        		new View.OnClickListener() {
			
					@Override
					public void onClick(View v) {
						
						find_a_slave();
					}
		});
        	
	}
	
    @Override
    public void onPause() {
        super.onPause();
    }
	
    
	private void initialize_wifiP2p() {
		context = this;
	    manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
	    channel = manager.initialize(this, getMainLooper(),
	    		new WifiP2pManager.ChannelListener() {
					@Override
					public void onChannelDisconnected() {	
						Log.d(TAG, "Host: OnChannelDisconnected!");											
					}});					
	}
	
	
	private void playListBrowseAction() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, CHOOSE_FILE_RESULT_CODE);
		
	}
	
    private void startService() {
        

        manager.discoverServices(channel, null);
    }

    @Override
    public void onStop() {
        super.onStop();
        //manager.clearLocalServices(channel,null);
    }
	
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) 
    {
    	Log.d(TAG, "onActivityResult..."+requestCode );
    	if(requestCode == CHOOSE_FILE_RESULT_CODE)
    	{
    		//just use it to set the text field for now
    		EditText playListURI = (EditText) findViewById(R.id.host_ET_browse_for_playlist);
    		Uri playList = data.getData();
    		String path = playList.getPath();
    		playListURI.setText(path);
    		
    	}
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.host_server_setup, menu);
		return true;
	}

}
