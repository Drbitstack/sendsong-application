package com.example.sendsong;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ClientHomeActivity extends Activity {

	public static final String INTENT_DATA_CLIENT_USERNAME = "hashkey.sendsong.client.USERNAME";
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.activity_client_home);
		
		setupActionBar();
		
	    Button continueButton = (Button) findViewById(R.id.continue_button);
	    
	    continueButton.setOnClickListener
	    (
	    	new View.OnClickListener()
	    	{
	    		public void onClick(View v)
	    		{
	    			continueButtonAction();
	    		}
	    	}
	    );
	    
	    

		
		//on button press of be a slave, go to slave server activity 
		
		
	}
	
	/******************************************************
	 * Function: continueButtonAction()
	 * 
	 * Description: This function reads in the entered username and sanitizes it
	 * before starting the next activity. 
	 * 
	 * Inputs: void
	 * 
	 * Outputs: void
	 * 
	 * Globals used: 
	 * 
	 */
	private void continueButtonAction()
	{
		//TODO validate field data before actually starting this activity and passing a shitty username
	    EditText username_view = (EditText) findViewById(R.id.username);
	    String username = username_view.getText().toString();
	    
	    
		if(username.length()>0){
			startClientActivity();
		}
		else {
			
			displayToast("LOL, NICE TRY!");
		}
	}
	
	private void displayToast(String message){
		
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}
	
	private void startClientActivity() {
		
	    Intent intent = new Intent(this, ClientServerChooseActivity.class);
	    
	    //get username
	    EditText username_view = (EditText) findViewById(R.id.username);
	    String username = username_view.getText().toString();
	    
	    //place username in intent message
	    intent.putExtra(INTENT_DATA_CLIENT_USERNAME, username);
	    startActivity(intent);

		
	}
	
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.client_home, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	

}
