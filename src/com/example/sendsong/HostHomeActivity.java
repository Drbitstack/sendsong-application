package com.example.sendsong;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class HostHomeActivity extends Activity {

	public static final String INTENT_DATA_HOST_USERNAME = "hashkey.sendsong.client.USERNAME";
	public static final String INTENT_DATA_HOST_PASSWORD = "hashkey.sendsong.client.PASSWORD";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_host_home);
		
		
		//set listener to login-> validate and go to next activity? This entire activity may not be needed
		//if we just use google authentication for Hosting privilege (to check for purchase)
		Button loginButton = (Button) findViewById(R.id.button_host_home_login);
		
		loginButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				loginButtonAction();
				
			}
		});
		
	}

	private void loginButtonAction()
	{
		
		Intent intent = new Intent(this, HostServerSetup.class);
		EditText enteredUsername = (EditText) findViewById(R.id.host_edittext_username);
		EditText enteredPassword = (EditText) findViewById(R.id.host_edittext_password);
		
		String username = sanitizeString(enteredUsername.getText());
		String password = sanitizeString(enteredPassword.getText());
		
		//salt & hash the data eventually
		
		//intent.putExtra(INTENT_DATA_HOST_USERNAME, username);
		//intent.putExtra(INTENT_DATA_HOST_PASSWORD, password);
		
//		if(validateCredentials(username,password)) 
			startActivity(intent);
//		else
//		{
//			Toast toast = Toast.makeText(this, "Invalid Credentials", Toast.LENGTH_SHORT);
//			toast.show();
//		}
		
		
	}
	
	private boolean validateCredentials(String username, String password) {
		
		if( username.equalsIgnoreCase("Admin") && password.equalsIgnoreCase("password")) return true;
		return false;
	}

	private String sanitizeString(Editable text) {
		// TODO Auto-generated method stub
		return text.toString();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.host_home, menu);
		return true;
	}

}
