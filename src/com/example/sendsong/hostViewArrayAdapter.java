package com.example.sendsong;

import java.util.List;

import android.content.Context;
import android.net.wifi.p2p.WifiP2pDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class hostViewArrayAdapter extends ArrayAdapter<WifiP2pDevice> {
	
	private final Context context;
	private final List<WifiP2pDevice> deviceList;

	public hostViewArrayAdapter(Context context, List<WifiP2pDevice> deviceList) {
		super(context,R.layout.default_listview_textview, deviceList);
		this.context = context;
		this.deviceList = deviceList;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
	    View rowView = inflater.inflate(R.layout.host_view_row_layout, parent, false);
	    //ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
	    TextView textViewLine1 = (TextView) rowView.findViewById(R.id.firstLine);
	    TextView textViewLine2 = (TextView) rowView.findViewById(R.id.secondLine);
	    	    
	    textViewLine1.setText("::" + deviceList.get(position).deviceName + "::");
	    textViewLine2.setText("::" + deviceList.get(position).deviceAddress + "::" );
	    	    
	    return rowView;	
	}

}
