package com.example.sendsong;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.WifiP2pManager.ConnectionInfoListener;
import android.net.wifi.p2p.WifiP2pManager.DnsSdServiceResponseListener;
import android.net.wifi.p2p.WifiP2pManager.ServiceResponseListener;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.os.Bundle;
import android.os.Looper;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

public class SlaveActivity extends Activity 
implements DnsSdServiceResponseListener, ConnectionInfoListener, ServiceResponseListener 
{
	

	private static final int SERVER_HOST_PORT = 6801;
	private static final int SERVER_CLIENT_PORT = 6802;
	
	private static final String TAG = "DUMMY";
	
	//FOR WIFI P2P
	private WifiP2pManager manager;
    private Channel channel;
    private Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_slave);
		// Show the Up button in the action bar.
		setupActionBar();
		
		initialize_wifiP2p();
	    
	    displayName(generateUniqueName());
	    //TODO: Perform service discovery & connect to the Host
	    startService();
	    //discoverService();
	       
	    //TODO: Start server for HOST ONLY connection. (not right here..)
	    //TODO: Start server for many client connections (again, not right here..)
	
		
	}

	private void displayName(String myName) {
		
		TextView tv = (TextView) findViewById(R.id.label_slave_name);
		tv.setText(myName);
	}

	private String generateUniqueName() {
		
		//TODO: actually generate the slave's name
		return "Leshawnamew Jeremiah Jones III";
	}

	private void initialize_wifiP2p() {
		context = this;
	    manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
	    channel = manager.initialize(this, getMainLooper(),
	    		new WifiP2pManager.ChannelListener() {
					@Override
					public void onChannelDisconnected() {	
						Log.d(TAG, "Host: OnChannelDisconnected!");											
					}});					
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.slave, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onServiceAvailable(int protocolType, byte[] responseData,
			WifiP2pDevice srcDevice) {
		// TODO Auto-generated method stub
		
	}
	
	
    private void startService() {
        //  Create a string map containing information about your service.
		Log.d(TAG, "startService... ");

        Map<String, String> record = new HashMap<String, String>();
        
        String serverName;
        
        EditText server = (EditText) findViewById(R.id.host_server_name_ET);
        //generate a name for myself
        final TextView serverStatus = (TextView) findViewById(R.id.host_tv_server_status);
        serverName = server.getText().toString();
        
        record.put("LISTEN_PORT", String.valueOf(SERVER_HOST_PORT)); //port to talk on
        record.put("SERVER_NAME", serverName);					//my server name
        record.put("SPARE", "visible");						//spare data

        // Service information.  Pass it an instance name, service type
        // _protocol._transportlayer , and the map containing
        // information other devices will want once they connect to this one.
        final WifiP2pDnsSdServiceInfo serviceInfo =
                WifiP2pDnsSdServiceInfo.newInstance("SendSongHostInstance", "_http._tcp", record);

        // Add the local service, sending the service info, network channel,
        // and listener that will be used to indicate success or failure of
        // the request.
        manager.clearLocalServices(channel, new ActionListener() {

			@Override
			public void onFailure(int reason) {
				Toast.makeText(context, "cleared services failed", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onSuccess() {
				Toast.makeText(context, "cleared services", Toast.LENGTH_SHORT).show();
				manager.addLocalService(channel, serviceInfo, new ActionListener() {
		            @Override
		            public void onSuccess() {
		    			Toast.makeText(context, "Service started", Toast.LENGTH_SHORT).show();
		    			//serverStatus.setText("Status: Up");
		            }

		            @Override
		            public void onFailure(int arg0) {
		    			Toast.makeText(context, "Service failed to start", Toast.LENGTH_SHORT).show();
		    			//serverStatus.setText("Status: Down");
		            }
		        });
			}
        });
    }
	
	public void connectP2p(WifiP2pDevice device) {
        WifiP2pConfig config = new WifiP2pConfig();
        config.groupOwnerIntent = 0;  //Less probability to become the GO
        config.deviceAddress = device.deviceAddress;
        config.wps.setup = WpsInfo.PBC;
        
        if (device != null)
            manager.removeServiceRequest(channel, serviceRequest,
                    new ActionListener() {

                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onFailure(int arg0) {
                        }
                    });

        manager.connect(channel, config, new ActionListener() {

            @Override
            public void onSuccess() {
            }

            @Override
            public void onFailure(int errorCode) {
            }
        });
    }

	@Override
	public void onConnectionInfoAvailable(WifiP2pInfo p2pInfo) {
        Thread handler = null;

        if (p2pInfo.isGroupOwner) {
            Log.d(TAG, "Connected as group owner");
            try {
                handler = new GroupOwnerSocketHandler( ((MessageTarget) this).getHandler());
                handler.start();
            } catch (IOException e) {
                Log.d(TAG,
                        "Failed to create a server thread - " + e.getMessage());
                return;
            }
        } else {
            Log.d(TAG, "Connected as peer");
            handler = new ClientSocketHandler(((MessageTarget) this).getHandler(), p2pInfo.groupOwnerAddress);
            handler.start();
        }
		
	}

	@Override
	public void onDnsSdServiceAvailable(String instanceName,
			String registrationType, WifiP2pDevice srcDevice) {
		// TODO Auto-generated method stub
		
	}

}
