package com.example.sendsong;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ListFragment;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager.ConnectionInfoListener;
import android.net.wifi.p2p.WifiP2pManager.DnsSdServiceResponseListener;
import android.net.wifi.p2p.WifiP2pManager.PeerListListener;
import android.net.wifi.p2p.WifiP2pManager.ServiceResponseListener;
import android.os.Bundle;
import android.util.Log;

public class PeerListFragment extends ListFragment 
implements DnsSdServiceResponseListener, PeerListListener, ConnectionInfoListener, ServiceResponseListener 
{
	
	final HashMap<String, String> detectedHosts = new HashMap<String, String>();
	protected static final String TAG = "PeerListFragment";
	
	public List<WifiP2pDevice> deviceList = new ArrayList<WifiP2pDevice>();
	private hostViewArrayAdapter adapter;
	
	//This fragment is notified about both service discovery and host discovery. 
	//A device populates this list only when it has been detected running the appropriate service.
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		//create the array
		
		//for(int i=0;i<10;i++)
		{
			WifiP2pDevice device = new WifiP2pDevice();
			device.deviceName = "Dummy Device Name";
			device.deviceAddress = "Dummy Device Address";
			deviceList.add(device);
		}
		//create the array adapter
		//set the adapter to the array	   
		Log.d(TAG, "PeerListFragment: onActivityCreated... ");
        adapter = new hostViewArrayAdapter(getActivity(), deviceList);
        setListAdapter(adapter);
	}
	
	@Override
	public void onPeersAvailable(WifiP2pDeviceList peers) {
//		deviceList.clear();
//		adapter.notifyDataSetChanged();
		Log.d(TAG, "onPeersAvailable... ");
		//deviceList.addAll(peers.getDeviceList());
		//adapter.notifyDataSetChanged();
	}

	@Override
	public void onConnectionInfoAvailable(WifiP2pInfo info) {
		// TODO Data after connection formed
		Log.d(TAG, "onConnectionInfoAvailable... ");
//		WifiP2pDevice dev = new WifiP2pDevice();
//		dev.deviceName = info.toString();
//		deviceList.add(dev);
//		adapter.notifyDataSetChanged();
	}
	
	@Override
	public void onDnsSdServiceAvailable(String instanceName, String registrationType,
            WifiP2pDevice resourceType) {

            // Update the device name with the human-friendly version from
            // the DnsTxtRecord, assuming one arrived.
            //resourceType.deviceName = buddies.containsKey(resourceType.deviceAddress) ? buddies.get(resourceType.deviceAddress) : resourceType.deviceName;

            // Add to the custom adapter defined specifically for showing
            // wifi devices.
            //WiFiDirectServicesList fragment = (WiFiDirectServicesList) getFragmentManager().findFragmentById(R.id.frag_peerlist);
            // WiFiDevicesAdapter adapter = ((WiFiDevicesAdapter) fragment.getListAdapter());
            
            //adapter.add(resourceType);
            //adapter.notifyDataSetChanged();
        	Log.d(TAG, "onDnsSdServiceAvailable " + instanceName);
            WifiP2pDevice deviceWithDNSService = new WifiP2pDevice();
            deviceWithDNSService.deviceName = resourceType.deviceName + 
            		(detectedHosts.containsKey(resourceType.deviceAddress) ? detectedHosts.get(resourceType.deviceAddress) : "; Name Not Found");
            deviceWithDNSService.deviceAddress = resourceType.deviceAddress;
            adapter.add(deviceWithDNSService);
            adapter.notifyDataSetChanged();

    }
	
	public void onDnsSdTxtRecordAvailable(String fullDomain, Map record, WifiP2pDevice device)
    {
        Log.d(TAG, "DnsSdTxtRecord available -" + record.toString());
        //extract data from service
        detectedHosts.put(device.deviceAddress, (String) record.get("SERVER_NAME")); //associate server name with device addr.
        detectedHosts.put("LISTEN_PORT",        (String) record.get("LISTEN_PORT")); //store listening port
    }

	@Override
	public void onServiceAvailable(int arg0, byte[] arg1, WifiP2pDevice arg2) {
		// TODO Auto-generated method stub
		Log.d(TAG, "onServiceAvailable");
		
	}



}
