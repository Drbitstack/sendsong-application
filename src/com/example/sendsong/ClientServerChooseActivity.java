package com.example.sendsong;

import java.util.Map;

import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.WifiP2pManager.ChannelListener;
import android.net.wifi.p2p.WifiP2pManager.DnsSdTxtRecordListener;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.net.wifi.p2p.nsd.WifiP2pServiceRequest;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Looper;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

public class ClientServerChooseActivity extends Activity  {
	
	
	/*
	 * GLOBALS:
	 */
	public String username;
	protected static final String TAG = "ClientHome";
	private final IntentFilter intentFilter = new IntentFilter();
	private boolean isWifiEnabled;
	
	//FOR WIFI P2P
	private WifiP2pManager manager;
    private boolean isWifiP2pEnabled = false;
    private Channel channel;
    private BroadcastReceiver receiver = null;
    private Looper mWFDLooper;

	/* END GLOBALS */
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_client);
		// Show the Up button in the action bar.
		setupActionBar();	
		Intent intent = getIntent();
		username = intent.getStringExtra(ClientHomeActivity.INTENT_DATA_CLIENT_USERNAME);
		
		//  Indicates a change in the Wi-Fi P2P status.
	    intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
	    // Indicates a change in the list of available peers.
	    intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
	    // Indicates the state of Wi-Fi P2P connectivity has changed.
	    intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
	    // Indicates this device's details have changed.
	    intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

	    manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
	    //HandlerThread handlerThread = new HandlerThread(TAG);
		//handlerThread.start();
		//mWFDLooper = handlerThread.getLooper();
	    channel = manager.initialize(this, getMainLooper(), 
	    		new WifiP2pManager.ChannelListener() {
					
					@Override
					public void onChannelDisconnected() {
						// TODO Auto-generated method stub
						Log.d(TAG, " Client: OnChannelDisconnected!");
						//mWFDLooper.quit();						
					}
				});
	    
	    //button actions
	    Button initiate_discovery_button = (Button) findViewById(R.id.initiate_discovery_button);
	    initiate_discovery_button.setOnClickListener
	    (
	    	new View.OnClickListener()
	    	{
	    		public void onClick(View v)
	    		{
	    			//Call discoverPeers method? Need to add a service request before discovery can begin
	    			//defineRequest();
	    			addRequest();
	    			setServiceListener();
	    			setDNsListener();
	    			//discoverPeers();
	    			discoverService();
	    		}
	    	}
	    );

	}
	


	private void addRequest() {
		
		WifiP2pDnsSdServiceRequest req = WifiP2pDnsSdServiceRequest.newInstance();
		manager.addServiceRequest(channel, req, null);
		
	}
	
	private void setServiceListener() {
		PeerListFragment peer_list_fragment = (PeerListFragment) (ClientServerChooseActivity.this).getFragmentManager().findFragmentById(R.id.peer_list_fragment);
		manager.setServiceResponseListener(channel, peer_list_fragment);
		
	}
	
	private void setDNsListener() {		
		PeerListFragment peer_list_fragment = (PeerListFragment) (ClientServerChooseActivity.this).getFragmentManager().findFragmentById(R.id.peer_list_fragment);
		manager.setDnsSdResponseListeners(channel, peer_list_fragment, new WifiP2pManager.DnsSdTxtRecordListener() {
			
			@Override
			public void onDnsSdTxtRecordAvailable(String arg0,
					Map<String, String> arg1, WifiP2pDevice arg2) {
				
				//don't do anything for the callback, handle in the peer_list_fragment for now.
				Log.d(TAG, "onDnsSdTxtRecordAvailable... ");
			}
		});
		
	}
	
	private void discoverPeers() {

		 manager.discoverPeers(channel, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
           	 displayToast("Peer Discovery Initiated");
            }

            @Override
            public void onFailure(int reasonCode) {	 
           	 displayToast("Discovery Failed : " + reasonCode);
            }
        });
		
	}

	private void discoverService() {

		 manager.discoverServices(channel, new WifiP2pManager.ActionListener() {

             @Override
             public void onSuccess() {
            	 displayToast("Service Discovery Initiated");
             }

             @Override
             public void onFailure(int reasonCode) {	 
            	 displayToast("Discovery Failed : " + reasonCode);
             }
         });
		
	}
	
    /** register the BroadcastReceiver with the intent values to be matched */
    @Override
    public void onResume() {
        super.onResume();
        receiver = new WiFiDirectBroadcastReceiver(manager, channel, this);
        registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
        //clear discovery requests in case no connection was made
//        manager.clearServiceRequests(channel, new WifiP2pManager.ActionListener(){
//
//			@Override
//			public void onFailure(int reason) {
//				// TODO Auto-generated method stub
//				
//			}
//
//			@Override
//			public void onSuccess() {
//				Log.d(TAG,"Cleared Service Requests");
//				
//			}});
//        manager.stopPeerDiscovery(channel, new WifiP2pManager.ActionListener() {
//			
//			@Override
//			public void onSuccess() {
//				// TODO Auto-generated method stub
//				
//			}
//			
//			@Override
//			public void onFailure(int reason) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
    }
    

	private void displayToast(String message){
		
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.client, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void setIsWifiP2pEnabled(boolean b) {
		this.isWifiEnabled = b;	
	}
	
	
}
