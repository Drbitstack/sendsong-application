package com.example.sendsong;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class OnLaunchActivity extends Activity {
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_on_launch);
		
		Typeface tf = Typeface.createFromAsset(getAssets(),"fonts/lottepaperfang.ttf");
		
		Button clientButton =  (Button) findViewById(R.id.button_choose_client);
		Button hostButton   =  (Button) findViewById(R.id.button_choose_host);
	    Button slaveButton  = (Button) findViewById(R.id.button_be_slave);
		clientButton.setTypeface(tf,Typeface.BOLD);
		hostButton.setTypeface(tf,Typeface.BOLD);
		
		clientButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				chooseClientAction();
			}
		});
		hostButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				chooseHostAction();
			}
		});
		
	    slaveButton.setOnClickListener
	    (
	    	new View.OnClickListener()
	    	{
	    		public void onClick(View v)
	    		{
	    			slaveButtonAction();
	    		}

	    	}
	    );


	}
	
	private void chooseHostAction() {
		
		Intent intent = new Intent(this, HostHomeActivity.class);
		
		startActivity(intent);
	}
	
	private void chooseClientAction() {
		
		Intent intent = new Intent(this, ClientHomeActivity.class);
		
		startActivity(intent);
		
	}
	
	/******************************************************
	 * Function: slaveButtonAction()
	 * 
	 * Description: Starts the process for this device to become an 
	 * 				audio streaming slave.
	 * 
	 * Inputs: void
	 * 
	 * Outputs: void
	 * 
	 * Globals used: 
	 * 
	 */
	private void slaveButtonAction() {

	    Intent intent = new Intent(this, SlaveActivity.class);

	    startActivity(intent);

		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.on_launch, menu);
		return true;
	}

}
